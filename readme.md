# Welcome to GRD-304

![Glenn Evans](src/glenn_photo.jpg)

My Name is Glenn Evans and I will be your Design for Multimedia instructor.
If you need to reach me regarding anything please use my college email below.
<gevans@stclaircollege.ca>

This foundation course is designed to give you an understanding of (in no particular order):

* CSS
* HTML
* SCSS
* Using git
* Image formats
* Image compression
* Sturcture of a URL
* Proper directory structure
* Design and layout of a website
* How to use Photoshop for designing websites
 

